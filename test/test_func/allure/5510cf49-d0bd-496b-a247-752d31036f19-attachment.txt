{
    "status_str": "ok",
    "status_code": 200,
    "message": "",
    "data": [
        {
            "id": 1,
            "name": "Device lvl 1",
            "seqnum": 1,
            "solutions": 1
        },
        {
            "id": 2,
            "name": "Text lvl 1",
            "seqnum": 2,
            "solutions": 0
        },
        {
            "id": 3,
            "name": "Single lvl 1",
            "seqnum": 3,
            "solutions": 1
        },
        {
            "id": 4,
            "name": "Multi lvl 1",
            "seqnum": 4,
            "solutions": 0
        }
    ]
}